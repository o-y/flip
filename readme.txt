*There isn't much of a documentation apart from this readme. Not in the sources or elsewhere. That is because I'm not satisfied with this level of operability and things tend to change this early in the development. Proper documentation and a neat runnable JAR file will be produced when the "future"-features have been finished.*

The "Flip" project - written in Java
---

It consist of one Java application and a programming API. Both are described below.

&nbsp;

� **FlipProject & FlipMemoryProject -classes**

Wraps Java Compiler API under easy-to-use programming interface.

* Compile .java files (to .class files) to disk
* Compile .java files (to .class files) to memory
* Specify dependencies in form of
 * Other .java files
 * Directories of class files
 * JAR files
* Run compiled .class files

&nbsp;

� **Flip -application**

Flip uses FlipMemoryProject to run .java files in memory. You can:

 * **Specify dependencies for compilation and running.**

	Same as with the programming API, but this time in the .java file itself.

&nbsp;

Usage examples include:

 * **Demonstrate your school work quicky by running:**

	`flip . my.package.Class`

	In this case you would have a file file such as  
	`./my/package/Class.java`

	And an alias for command `flip` to something along the lines of  
	`java $flipClass -classpath $flipClassFolder $1*`

	� Please note that when the "future"-features are finished, FlipProject will be one small JAR file.

 * **Create build files for Java projects written in Java.**

	It is my opinion that Java is more than capable language for build files.

&nbsp;

In the case of Flip, the dependencies can be specified in the source file, like so:

<pre><code class="java">package gima.apps.flip.tests;

import compiletests.TestCompileDep2;
import libclasstests.TestLibClassDep;
import libjartests.TestLibJarDep;

/* FLIP DEPENDENCIES:
compile(.):
	gima.apps.flip.tests.TestCompileDep
	
compile(../tests/):
	compiletests.TestCompileDep2

lib:
	../tests/libjartests/TestLibJar.jar
	../tests/
*/

public class TestEntryClass {
	
	public static void main(String[] args) {
		System.out.println("Test 1/5 successful (TestEntryClass)");
		TestCompileDep.test();
		TestCompileDep2.test();
		TestLibClassDep.test();
		TestLibJarDep.test();
	}
	
}
</code></pre>

&nbsp;

Future
-

I will try to integrate these in the future. I would say near future, but that pressure wouldn't do me (or the project) any good.

* **Ability to produce a package from the output folder, with dependencies and resources (One-JAR, for example)**

	As long as One-JAR supports integration, which for some reason I believe it does :)

* **Native library support**

	No magic here. Tweaking the ClassLoader a bit, unless URLClassLoader already supports native libraries inside JAR files, which I don't remember reading anything about, thus probably not.

* **Resources support**

	Will do.

* **Strip unneeded class files**

	Strip may be the wrong word. Since during compilation, the Java compiler needs to read all the .java and .class files necessary for runtime as well, it should be easy to keep track of them. But in the case the code relies on Reflection to use classes that weren't needed during compilation, those pieces of code would fail. Of course those classes could be added manually.


<link rel="stylesheet" href="http://yandex.st/highlightjs/6.1/styles/github.css"></link>
<script src="http://yandex.st/highlightjs/6.1/highlight.min.js"></script>
<script type="text/javascript">
    hljs.initHighlightingOnLoad();
</script>
