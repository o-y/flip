/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package flipp.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.tools.StandardLocation;

import murprum.Murprum;
import murprum.file.FileHelper;
import murprum.stream.StreamHelper;
import murprum.sys.S;
import flipp.exceptions.CompilerNotFoundException;

/**
 * 
 * 
 * @author Gima
 */
public class FlipDirProject extends FlipBaseProject {

	private final File absProjectDir;
	private File absClassOutDir;
	private File absSrcOutDir;
	private File absLibOutDir;
	private File absResOutDir;
	private File absNativeOutDir;

	public FlipDirProject(String projectDir) throws CompilerNotFoundException {
		super();
		
		if (projectDir == null) throw new NullPointerException("projectDir must not be null");
		
		this.absProjectDir = new File(projectDir).getAbsoluteFile();
		if (!absProjectDir.isDirectory()) {
			if (absProjectDir.exists()) throw new Error(S.sprintf("Given projectDir is not a directory: %s", projectDir.toString()));
			if (!absProjectDir.mkdirs()) throw new Error(S.sprintf("Given projectDir didn't exist and unable to create it: %s", projectDir.toString()));
		}
		
		setClassOutDir("bin");
		setSrcOutDir("src");
		setLibOutDir("lib");
		setResOutDir("res");
		setNativeOutDir("native");
	}
	
	public void cleanProjectDir() throws FileNotFoundException {
		if (Murprum.isDebug()) S.debugFunc("%s", absProjectDir);
		FileHelper.deleteDirContent(absProjectDir);
	}

	@Override
	public void compile() throws Exception {
		absClassOutDir.mkdir();
		absSrcOutDir.mkdir();
		
		setCompilerSettings();

		setLocation(StandardLocation.CLASS_OUTPUT, getClassOutDir(), "Compilation class");
		
		if (Murprum.isDebug()) S.printf("<FlipProject Adding> compile-time annotation processor path to classloader");
		getAnnotationProcessorClassLoader().addURL(getClassOutDir().toURI().toURL());
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		setLocation(StandardLocation.SOURCE_OUTPUT, getSrcOutDir(), "Source");

		super.compile();
		
		// delete empty directories
		absClassOutDir.delete();
		absSrcOutDir.delete();
	}

	/**
	 * If entryClassName is not null, copies specified class dependencies, native libraries, resources and
	 * a launcher to the projectDir.
	 * 
	 * @param entryClassName - Fully qualified class name to set as main class in manifest, null is none should be set.
	 */
	public void finishProjectDir(String entryClassName) throws Exception {
		if (Murprum.isDebug()) S.funcArgs(entryClassName);
		
		absLibOutDir.mkdir();
		absResOutDir.mkdir();
		absNativeOutDir.mkdir();
		
		if (entryClassName == null) {
			FileHelper.moveAll(absClassOutDir, absProjectDir); 
		}
		else {
			Manifest manifest = new Manifest();
			Attributes attributes = manifest.getMainAttributes();
			attributes.put(Attributes.Name.MANIFEST_VERSION, "1.0");
			
			// copy classpath entries to projectDir and write them to manifest
			LinkedHashSet<String> manifestClassPathEntries = new LinkedHashSet<String>();
			
			if (Murprum.isDebug()) S.printf("<Adding> classpath entries to projectDir");
			for (String classPathEntry : getSettings().getClassPathEntries()) {
				File classPathEntryFile = new File(classPathEntry);
				File outLibFile = new File(absLibOutDir, classPathEntryFile.getName());
				String relOutLibString = absProjectDir.toURI().relativize(outLibFile.toURI()).toString();
				
				if (classPathEntryFile.isDirectory()) {
					FileHelper.copyDirContent(classPathEntryFile, outLibFile);
					if (Murprum.isDebug()) S.printf("- [DIR ] %s", relOutLibString);
				}
				else if (classPathEntryFile.isFile()) {
					FileHelper.copyFileToFile(classPathEntryFile, outLibFile);
					if (Murprum.isDebug()) S.printf("- [FILE] %s", relOutLibString);
				}
				else continue;
				
				manifestClassPathEntries.add(relOutLibString);
			}
			if (Murprum.isDebug()) S.printf("</Done>%n");
			
			// same for resources, though i think only directories should be handled
			// resource for current directory doesn't need to be added,
			// "." is added by default if i remember correctly
			// .. i read there was some problem on mac and classpath
			if (Murprum.isDebug()) S.printf("<Adding> resource entries to projectDir");
			for (String resourceEntry : getSettings().getResources()) {
				File resourceEntryFile = new File(resourceEntry);
				File outResFile = new File(absResOutDir, resourceEntryFile.getName());
				String relOutResString;
				
				if (resourceEntryFile.isDirectory()) {
					relOutResString = absProjectDir.toURI().relativize(absResOutDir.toURI()).toString();
					if (Murprum.isDebug()) S.printf("- %s", relOutResString);
					FileHelper.copyDirContent(resourceEntryFile, outResFile);
				}
				else if (resourceEntryFile.isFile()) {
					relOutResString = absProjectDir.toURI().relativize(outResFile.toURI()).toString();
					if (Murprum.isDebug()) S.printf("- %s", relOutResString);
					FileHelper.copyFileToFile(resourceEntryFile, outResFile);
				}
				else continue;
				
				manifestClassPathEntries.add(relOutResString);
			}
			if (Murprum.isDebug()) S.printf("</Done>%n");
			
			// manufacture manifest "Class-Path" value
			/*if (Murprum.isDebug()) S.printf("<Setting> classpath value to manifest in projectDir");
			if (manifestClassPathEntries.size() > 0) {
				StringBuilder manifestClassPathValue = new StringBuilder();
				
				for (String classPathEntry : manifestClassPathEntries) {
					if (classPathEntry.contains(" ")) {
						manifestClassPathValue.append("\"");
						manifestClassPathValue.append(classPathEntry);
						manifestClassPathValue.append("\"");
					}
					else {
						manifestClassPathValue.append(classPathEntry);
					}
					
					manifestClassPathValue.append(" ");
				}
				
				manifestClassPathValue.setLength(manifestClassPathValue.length() - 1);
				if (Murprum.isDebug()) S.printf("%s", manifestClassPathValue.toString());
				attributes.put(Attributes.Name.CLASS_PATH, manifestClassPathValue.toString());
			}
			if (Murprum.isDebug()) S.printf("</Done>%n");*/
			
			
			if (Murprum.isDebug()) S.printf("<Adding> native libraries to projectDir");
			for (Entry<String, LinkedList<String>> libEntry : getSettings().getNativeLibDirs().entrySet()) {
				String osIdentifier = libEntry.getKey();
				
				// copy dirs to osIdentifier folder
				for (String libDir : libEntry.getValue()) {
					File outLibDir = new File(absNativeOutDir, osIdentifier);
					
					if (!outLibDir.isDirectory()) {
						if (!outLibDir.mkdir()) {
							throw new Exception(S.sprintf(
									"Failed to create native library directory (%s)",
									outLibDir.toString()));
						}
					} // if
	
					FileHelper.copyDirContent(new File(libDir), outLibDir);
				}
			}
			if (Murprum.isDebug()) S.printf("</Done>%n");
			
			attributes.put(Attributes.Name.MAIN_CLASS, entryClassName);
			
			if (Murprum.isDebug()) S.printf("<Adding> launcher to projectDir");
			File flipJarFile = new File(FileHelper.getCodeRoot());
			if (!flipJarFile.isFile()) {
				throw new Exception("Could not create launcher because Flip was not launcher from it's .jar file (Flip is the launcher as well)");
			}
			File launcherJarFile = new File(absProjectDir, "Launcher.jar");
			FileHelper.copyFileToFile(flipJarFile, launcherJarFile);
			if (Murprum.isDebug()) S.printf("</Done>%n");
		
			if (Murprum.isDebug()) S.printf("<Adding> manifest to projectDir");
			File metaInfDir = new File(absProjectDir, "META-INF");
			if (!metaInfDir.mkdir()) {
				throw new IOException("Failed to create 'META-INF' directory under projectDir");
			}
			File manifestFile = new File(metaInfDir, "MANIFEST.MF");
			if (Murprum.isDebug()) S.printf("%s", manifestFile.toString());
			FileOutputStream metaInfOutStream = new FileOutputStream(manifestFile);
			manifest.write(metaInfOutStream);
			metaInfOutStream.close();
			if (Murprum.isDebug()) S.printf("</Done>%n");
		}
		
		// delete empty directories
		absClassOutDir.delete();
		absSrcOutDir.delete();
		absLibOutDir.delete();
		absResOutDir.delete();
		absNativeOutDir.delete();
		
	} // method
	
	public void jar(String jarFileName) throws IOException {
		if (Murprum.isDebug()) S.funcArgs(jarFileName);
		
		List<File> releaseFiles = FileHelper.listFiles(absProjectDir);
		ZipOutputStream zipOutStream = new ZipOutputStream(new FileOutputStream(new File(jarFileName)));
		
		URI releaseDirURI = absProjectDir.toURI();
		
		if (Murprum.isDebug()) S.printf("<Adding> release content to JAR file");
		for (File releaseFile : releaseFiles) {
			String entryRelToProjRoot = releaseDirURI.relativize(releaseFile.toURI()).toString();
			if (Murprum.isDebug()) {
				S.printf("- %s", releaseFile.toString());
				S.printf("    (as) %s", entryRelToProjRoot);
			}
			ZipEntry zipEntry = new ZipEntry(entryRelToProjRoot);
			zipEntry.setMethod(ZipEntry.DEFLATED);
			zipOutStream.putNextEntry(zipEntry);
			
			FileInputStream releaseFileInStream = new FileInputStream(releaseFile);
			StreamHelper.transferTo(releaseFileInStream, zipOutStream);
			releaseFileInStream.close();
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		zipOutStream.close();
	}
	
	public File getProjectDir() {
		return absProjectDir;
	}

	private void setClassOutDir(String dir) {
		absClassOutDir = new File(absProjectDir, dir).getAbsoluteFile();
	}
	
	public File getClassOutDir() {
		return absClassOutDir;
	}
	
	private void setSrcOutDir(String dir) {
		absSrcOutDir = new File(absProjectDir, dir).getAbsoluteFile();
	}
	
	public File getSrcOutDir() {
		return absSrcOutDir;
	}

	private void setLibOutDir(String dir) {
		absLibOutDir = new File(absProjectDir, dir).getAbsoluteFile();
	}
	
	public File getLibOutputDir() {
		return absLibOutDir;
	}
	
	private void setResOutDir(String dir) {
		absResOutDir = new File(absProjectDir, dir).getAbsoluteFile();
	}
	
	public File getResOutDir() {
		return absResOutDir;
	}
	
	private void setNativeOutDir(String dir) {
		absNativeOutDir = new File(absProjectDir, dir).getAbsoluteFile();
	}
	
	public File getNativeOutDir() {
		return absNativeOutDir;
	}
	
}
