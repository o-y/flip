/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.project;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import murprum.Murprum;
import murprum.sys.S;

public class FlipProjectSettings {
	private final LinkedHashSet<String> srcRootDirs;
	private final LinkedHashSet<String> classesToCompile;
	private final LinkedHashSet<String> classPathEntries;
	private final LinkedHashSet<String> annotationProcessorRootDirs;
	private final LinkedHashSet<String> annotationProcessors;
	private final LinkedList<String> compilerOptions;

	private final LinkedHashSet<String> resources;
	private final LinkedHashMap<String, LinkedList<String>> nativeLibDirs;
	
	public FlipProjectSettings() {
		srcRootDirs = new LinkedHashSet<String>();
		classPathEntries = new LinkedHashSet<String>();
		classesToCompile = new LinkedHashSet<String>();
		annotationProcessorRootDirs = new LinkedHashSet<String>();
		annotationProcessors = new LinkedHashSet<String>();
		compilerOptions = new LinkedList<String>(Arrays.asList(new String[] { "" }));

		resources = new LinkedHashSet<String>();
		nativeLibDirs = new LinkedHashMap<String, LinkedList<String>>();
	}
	
	@SuppressWarnings("unused")
	private void separator5() {}

	public void setCompilerOptions(String... compilerOptions) {
		if (Murprum.isDebug()) S.funcArgs("...");
		LinkedList<String> newCompilerOptions = new LinkedList<String>();
		
		for (String compilerOption : Arrays.asList(compilerOptions)) {
			newCompilerOptions.add(compilerOption);
		}
		this.compilerOptions.clear();
		if (Murprum.isDebug()) S.printf("<Listing> new compiler options");
		S.sprintf("%s", newCompilerOptions.toString());
		if (Murprum.isDebug()) S.printf("</Done>%n");
		this.compilerOptions.addAll(newCompilerOptions);
	}
	
	public String[] getCompilerOptions() {
		return compilerOptions.toArray(new String[0]);
	}
	
	@SuppressWarnings("unused")
	private void separator3() {}

	public void addResources(List<String> resources) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> resources about to be added");
		for (String res : resources) {
			S.printf("- %s", res);
			this.resources.add(res);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #addResources(List)} */
	public void addResource(String resource) {
		if (Murprum.isDebug()) S.funcArgs(resource);
		addResources(Arrays.asList(new String[] { resource }));
	}
	
	public LinkedHashSet<String> getResources() {
		return new LinkedHashSet<String>(resources);
	}
	
	@SuppressWarnings("unused")
	private void separator4() {}

	public void addSrcRootDirs(List<String> srcRootDirs) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> source root dirs about to be added");
		for (String srcRootDir : srcRootDirs) {
			if (Murprum.isDebug()) S.printf("- %s", srcRootDir);
			this.srcRootDirs.add(srcRootDir);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #addSrcRootDirs(List)} */
	public void addSrcRootDir(String srcRootDir) {
		addSrcRootDirs(Arrays.asList(new String[] { srcRootDir }));
	}
	
	public LinkedHashSet<String> getSrcRootDirs() {
		return srcRootDirs;
	}
	
	@SuppressWarnings("unused")
	private void separator1() {}
	
	/** Supports only directories and .jar files. */
	public void addClassPathEntries(List<String> classPathEntries) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> classpath entries about to be added");
		for (String classPathEntry : classPathEntries) {
			if (Murprum.isDebug()) S.printf("- %s", classPathEntry);
			this.classPathEntries.add(classPathEntry);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		new Throwable().printStackTrace();
	}
	
	/** Helper function for {@link #addClassPathEntries(List)} */
	public void addClassPathEntry(String classPathEntry) {
		addClassPathEntries(Arrays.asList(new String[] { classPathEntry }));
	}

	public LinkedHashSet<String> getClassPathEntries() {
		return classPathEntries;
	}
	
	@SuppressWarnings("unused")
	private void separator2() {}

	public void addAnnotationProcessorRootDirs(List<String> annotationProcessorRootDirs) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> annotation processor root dirs about to be added");
		for (String annotationProcessorRootDir : annotationProcessorRootDirs) {
			this.annotationProcessorRootDirs.add(annotationProcessorRootDir);
			if (Murprum.isDebug()) S.printf("- %s", annotationProcessorRootDir);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #addAnnotationProcessorRootDirs(List)} */
	public void addAnnotationProcessorRootDir(String annotationProcessorRootDir) {
		addAnnotationProcessorRootDirs(
				Arrays.asList(new String[] { annotationProcessorRootDir })
				);
	}
	
	public LinkedHashSet<String> getAnnotationProcessorRootDirs() {
		return annotationProcessorRootDirs;
	}
	
	@SuppressWarnings("unused")
	private void separator7() {}

	public void addAnnotationProcessors(List<String> classNames) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> annotation processors about to be added");
		for (String className : classNames) {
			annotationProcessors.add(className);
			if (Murprum.isDebug()) S.printf("- %s", className);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #addAnnotationProcessors(List)} */
	public void addAnnotationProcessor(String className) {
		addAnnotationProcessors(Arrays.asList(new String[] { className }));
	}
	
	public LinkedHashSet<String> getAnnotationProcessors() {
		return annotationProcessors;
	}
	
	@SuppressWarnings("unused")
	private void separator6() {}
	
	public void addNativeLibDirs(LinkedHashMap<String, LinkedList<String>> nativeLibDirs) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> native library directories about to be added");
		for (String osIdentifier : nativeLibDirs.keySet()) {
			for (String nativeLibDir : nativeLibDirs.get(osIdentifier)) {
				LinkedList<String> currentDirs = this.nativeLibDirs.get(osIdentifier);
				if (currentDirs == null) {
					currentDirs = new LinkedList<String>();
					this.nativeLibDirs.put(osIdentifier, currentDirs);
				}
				currentDirs.add(nativeLibDir);
				if (Murprum.isDebug()) S.printf("- [%s] %s", osIdentifier, nativeLibDir);
			}
		} // for
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}

	public void addNativeLibDir(String nativeLibDir, String osIdentifier) {
		LinkedHashMap<String, LinkedList<String>> nativeLibDirs = new LinkedHashMap<String, LinkedList<String>>();
		nativeLibDirs.put(osIdentifier, new LinkedList<String>());
		nativeLibDirs.get(osIdentifier).add(nativeLibDir);
		addNativeLibDirs(nativeLibDirs);
	}
	
	public LinkedHashMap<String, LinkedList<String>> getNativeLibDirs() {
		return nativeLibDirs;
	}
	
	@SuppressWarnings("unused")
	private void separator8() {}
	
	public void addClassesToCompile(List<String> classNames) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> classes to be compiled about to be added");
		for (String className : classNames) {
			classesToCompile.add(className);
			if (Murprum.isDebug()) S.printf("- %s", className);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #addClassesToCompile(List)} */
	public void addClassToCompile(String className) {
		addClassesToCompile(Arrays.asList(new String[] { className }));
	}
	
	public LinkedHashSet<String> getClassesToCompile() {
		return classesToCompile;
	}
	
	@SuppressWarnings("unused")
	private void separator9() {}
	
	public void removeClassesFromCompile(List<String> classNamesToRemove) {
		if (Murprum.isDebug()) S.funcArgs("...");
		if (Murprum.isDebug()) S.printf("<Listing> classes to be removed from compilation");
		for (String className : classNamesToRemove) {
			if (Murprum.isDebug()) S.printfn("- %s", className);
			if (classesToCompile.remove(classNamesToRemove)) {
				S.printf(" [OK]");
			}
			else {
				S.printf(" [NOT OK: didn't exist]");
			}
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	/** Helper function for {@link #removeClassesFromCompile(List)} */
	public void removeClassFromCompile(String classNameToRemove) {
		removeClassesFromCompile(Arrays.asList(new String[] { classNameToRemove }));
	}
	
}