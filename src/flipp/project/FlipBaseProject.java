/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.project;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedList;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import murprum.Murprum;
import murprum.file.FileHelper;
import murprum.java.ClassHelper;
import murprum.sys.S;
import flipp.compiler.FlipClassLoader;
import flipp.compiler.FlipJavaFileManager;
import flipp.exceptions.CompilationFailedException;
import flipp.exceptions.CompilerNotFoundException;
import flipp.exceptions.MainMethodNotFoundException;

// TODO: test with annotation processor

/**
 * 
 * 
 * @author Gima
 */
class FlipBaseProject {
	
	private final JavaCompiler javaCompiler;
	private final FlipProjectSettings settings;
	private final FlipJavaFileManager flipJFM;
	private final ArrayList<JavaFileObject> compilationUnits;
	
	protected FlipBaseProject() throws CompilerNotFoundException {
		javaCompiler = ToolProvider.getSystemJavaCompiler();
		if (javaCompiler == null) throw new CompilerNotFoundException("Java compiler not available. Running under JRE virtual machine, not JDK's?");

		settings = new FlipProjectSettings();
		flipJFM = new FlipJavaFileManager(javaCompiler.getStandardFileManager(null, null, null));
		compilationUnits = new ArrayList<JavaFileObject>();
	}
	
	protected void setCompilerSettings() throws Exception {
		if (Murprum.isDebug()) S.funcArgs();
		
		setSourceRootDirs();
		setClassesToCompile();
		setClassPath();
		setAnnotationProcessorRootDirs();
		
		if (Murprum.isDebug()) {
//			S.printf("<Listing> annotation processors");
//			for (String annotationProcessor : getSettings().getAnnotationProcessors()) {
//				S.printf("- %s", annotationProcessor);
//			}
//			S.printf("</Done>%n");

			S.printf("Java Compiler options: %s%n", Arrays.asList(getSettings().getCompilerOptions()).toString());
		}
	}
	
	public FlipProjectSettings getSettings() {
		return settings;
	}
	
	protected void setLocation(StandardLocation location, File dirFile, String displayName) throws Exception {
		if (Murprum.isDebug()) S.funcArgs(location, dirFile, displayName);
		if (Murprum.isDebug()) S.printf("<Setting> %s output directory", displayName);
		
//		if (dirFile.exists()) {
//			if (!dirFile.isDirectory()) {
//				throw new Exception(
//						S.sprintf("%s subdirectory exists, but is not a directory: %s", displayName, dirFile.toString())
//						);
//			}
//		}
//		else {
//			if (!dirFile.mkdirs()) {
//				throw new Exception(
//						S.sprintf("Failed to create %s directory: %s", displayName, dirFile.toString())
//						);
//			}
//		}
		
		ArrayList<File> outputDir = new ArrayList<File>();
		outputDir.add(dirFile);
		
		if (Murprum.isDebug()) S.printf("%s", outputDir.get(0));
		flipJFM.setLocation(location, outputDir);
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	protected void setLocationToMemory(Location location, boolean b) {
		if (Murprum.isDebug()) S.funcArgs(location, b);
		flipJFM.setLocationHandled(location, b);
	}

	protected void compile() throws Exception {
		if (Murprum.isDebug()) {
			S.funcArgs();
			S.printf("#");
			S.printf("# Now, compile:");
			S.printf("#%n");
		}
		
//		compile
		
		CompilationTask compilationTask = javaCompiler.getTask(null, flipJFM, null, Arrays.asList(getSettings().getCompilerOptions()), getSettings().getAnnotationProcessors(), compilationUnits);
		if (compilationTask.call() == false) {
			throw new CompilationFailedException("");
		}
		
//		if (Murprum.isDebug()) S.printf("<FlipProjectBase flushing>");
//		try {
//			flipJFM.flush();
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		debugCompilationSuccessfull();
	}
	
	public void runMain(String className, String[] args) throws MalformedURLException, ClassNotFoundException, MainMethodNotFoundException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, URISyntaxException {
		if (Murprum.isDebug()) S.funcArgs();
		
		FlipClassLoader flipClassLoader = (FlipClassLoader) flipJFM.getClassLoader(StandardLocation.CLASS_OUTPUT);

		// remember the "bug" when in reality URLClassLoader specifically stated that only URI's ending in slash are interpreted as directories
		// though non-existing File's .toURI spouts slash-prefixless entry when the underlying file is not a directory.. or doesn't exist yet,
		// but that was when classPathEntries was an URI.
		// now it's the end-user who doesn't understand when he manages to delete one of the generated or specified classpath entry folders
		// before hitting compile and is taunted by a ClassNotFoundException saying it doesn't find a class when the debug-info clearly shows
		// that such a folder was added just moments ago. it's just the small magical slash missing from the end trololol.
		// though the possibility of someone accidentally ending up with folder in the classPathEntries which doesn't exist right now is
		// quite close to zero. they need to specifically add it (in which case it already exists) (or it's added knowing the compiled class
		// binaries are needed from that folder) and if they manage to delete in after that and before hitting compile, well, though luck
		
		if (Murprum.isDebug()) S.printf("<Adding> runtime classpath entries");
		for (String classPathEntry : getSettings().getClassPathEntries()) {
			if (Murprum.isDebug()) S.printf("- %s", classPathEntry);
			flipClassLoader.addURL(new File(classPathEntry).toURI().toURL());
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		File codeRoot = new File(FileHelper.getCodeRoot());
		if (Murprum.isDebug()) S.printf("<Adding> code root to runtime classpath");
		if (Murprum.isDebug()) S.printf("- %s", codeRoot);
		flipClassLoader.addURL(codeRoot.toURI().toURL());
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Adding> runtime resource entries");
		for (String resourceEntry : getSettings().getResources()) {
			if (Murprum.isDebug()) S.printf("- %s", resourceEntry);
			flipClassLoader.addURL(new File(resourceEntry).toURI().toURL());
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
//		if (Murprum.isDebug()) S.printf("<FlipProjectBase Adding> runtime native library root directories: ");
//		for (String osIdentifier : getSettings().getNativeLibDirs().keySet()) {
//			if (Murprum.isDebug()) S.printf("[%s]", osIdentifier);
//			for (String libDir : getSettings().getNativeLibDirs().get(osIdentifier)) {
//				flipClassLoader.addNativeLibRootDir(osIdentifier);
//			}
//		}
//		if (Murprum.isDebug()) S.printf("</Adding>%n");
//		
//		if (Murprum.isDebug()) S.printf("<FlipProjectBase Listing> runtime native library directories: ");
//		for (File nativeLibraryDirFile : flipClassLoader.getNativeLibraryDirs()) {
//			if (Murprum.isDebug()) S.printf("- %s", nativeLibraryDirFile.toURI());
//		}
//		if (Murprum.isDebug()) S.printf("</Listing>%n");
		
		if (Murprum.isDebug()) {
			S.printf("<Listing> runtime native libraries: ");
			for (String osIdentifier : getSettings().getNativeLibDirs().keySet()) {
				S.printf("[%s]", osIdentifier);
				for (String libDir : getSettings().getNativeLibDirs().get(osIdentifier)) {
					S.printf(" - %s", libDir);
				}
			}
			S.printf("</Done>%n");
		}
		
		Class<?> entryClass = flipClassLoader.loadClass(className);
		
		Method mainMethod = ClassHelper.getMainMethod(entryClass);
		if (mainMethod == null) {
			throw new MainMethodNotFoundException(
					S.sprintf("%s", "Could not execute .java file, could not find: 'public static void main(String[] args)'.")
					);
		}

		if (Murprum.isDebug()) S.printf(
				"About to run \"public static void main(String[] args)\" method of class: %s%n",
				entryClass.getName());
		
		// execute entry class's main method
		mainMethod.invoke(entryClass, (Object) args);
	}
	
	private void setSourceRootDirs() throws IOException {
		if (Murprum.isDebug()) S.funcArgs();
		
		LinkedList<File> sourceRootDirFiles = new LinkedList<File>();
		
		if (Murprum.isDebug()) S.printf("<Listing> source directories");
		for (String sourceDir : getSettings().getSrcRootDirs()) {
			if (Murprum.isDebug()) S.printf("- %s", sourceDir);
			sourceRootDirFiles.add(new File(sourceDir));
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Setting> location for source root directories");
		flipJFM.setLocation(StandardLocation.SOURCE_PATH, sourceRootDirFiles);
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Listing> source java files known to StandardJavaFileManager");
		for (JavaFileObject javaFileObject : flipJFM.list(StandardLocation.SOURCE_PATH, "", EnumSet.of(Kind.SOURCE), true)) {
			if (Murprum.isDebug()) S.printf("- %s", javaFileObject.toUri());
//				File absJavaFileFileObject = new File(javaFileObject.toUri().normalize());
//				String absJavaFileObjectString = absJavaFileFileObject.toString();
//				
//				File shortestRootDirectoryFile = null;
//				
//				for (File absSourceRootDirectory : sourceRootDirs) {
//					String absSourceRootDirectoryString = absSourceRootDirectory.toString();
//					
//					if (absJavaFileObjectString.startsWith(absSourceRootDirectoryString)) {
//						if (shortestRootDirectoryFile == null) shortestRootDirectoryFile = absSourceRootDirectory;
//						else if (absSourceRootDirectoryString.length() < absJavaFileObjectString.length()) {
//							shortestRootDirectoryFile = absSourceRootDirectory;
//						}
//					}
//				}
//				
//				if (shortestRootDirectoryFile == null) {
//					S.eprintf("StandardMemoryFileManager knows of a source file, but a source directory wasn't added for it (this shouldn't happen..)");
//					S.eprintf("Source file: %s", absJavaFileObjectString);
//					S.eprintf("<Listing> source directories");
//					for (File sourceDirectory : sourceRootDirs) {
//						S.eprintf("- %s", sourceDirectory.toString());
//					}
//					S.eprintf("</Listing>%n");
//					return false;
//				}
//					
//				if (Murprum.isDebug()) S.printf("- %s", absJavaFileObjectString);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
	}
	
	private void setClassesToCompile() throws Exception {
		if (Murprum.isDebug()) S.funcArgs();
		
		// add class names to be compiled
		if (Murprum.isDebug()) S.printf("<Adding> class names to be compiled as compilation units");
		for (String sourceClassName : getSettings().getClassesToCompile()) {
			if (Murprum.isDebug()) S.printf("- %s", sourceClassName);
			
			JavaFileObject javaFileObject = getSourceFileObjectForCompilation(sourceClassName);
			
			compilationUnits.add(javaFileObject);
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");

	}

	private JavaFileObject getSourceFileObjectForCompilation(String sourceClassName) throws Exception {
		JavaFileObject javaFileObject = flipJFM.getJavaFileForInput(StandardLocation.SOURCE_PATH, sourceClassName, Kind.SOURCE);
		if (javaFileObject == null) {
//			S.eprintf("Source file doesn't exist in any of the specified source locations.");
			S.eprintf("Source file: %s", sourceClassName);
			S.eprintf("Source file locations:");
			for (String sourceBaseDir : getSettings().getSrcRootDirs()) {
				S.eprintf("- %s", sourceBaseDir);
			}
			throw new Exception("Source file doesn't exist in any of the specified source locations.");
		}
		return javaFileObject;
	}
	
	private void setClassPath() throws IOException, URISyntaxException {
		if (Murprum.isDebug()) S.funcArgs();
		
		LinkedList<File> classPathEntryFiles = new LinkedList<File>();
		
		File codeRoot = new File(FileHelper.getCodeRoot());
		if (Murprum.isDebug()) S.printf("<Adding> runtime classpath entry for code root");
		if (Murprum.isDebug()) S.printf("- %s", codeRoot);
		classPathEntryFiles.add(codeRoot);
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Listing> compile-time classpath entries");
		for (String classPathEntry : getSettings().getClassPathEntries()) {
			if (Murprum.isDebug()) S.printf("- %s", classPathEntry);
			classPathEntryFiles.add(new File(classPathEntry));
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Setting> classpath");
		flipJFM.setLocation(StandardLocation.CLASS_PATH, classPathEntryFiles);
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		// list compile-time classpath entries
		if (Murprum.isDebug()) {
			S.printf("<Listing> possible classes from StandardJavaFileManager");
			for (JavaFileObject javaFileObject : flipJFM.list(StandardLocation.CLASS_PATH, "", EnumSet.of(Kind.CLASS), true)) {
				S.printf("- %s", javaFileObject.toUri());
			}
			S.printf("</Done>%n");
		}
	}
	
	private void setAnnotationProcessorRootDirs() throws IOException {
		if (Murprum.isDebug()) S.funcArgs();
		
		LinkedList<File> annotationProcessorRootDirs = new LinkedList<File>();

		if (Murprum.isDebug()) S.printf("<Listing> annotation processor root directories");
		for (String annotationProcessorRootDir : getSettings().getAnnotationProcessorRootDirs()) {
			S.printf("- %s", annotationProcessorRootDir);
			File annotationProcessorRootDirFile = new File(annotationProcessorRootDir);
			annotationProcessorRootDirs.add(annotationProcessorRootDirFile);
			getAnnotationProcessorClassLoader().addURL(annotationProcessorRootDirFile.toURI().toURL());
		}
		if (Murprum.isDebug()) S.printf("</Done>%n");
		
		if (Murprum.isDebug()) S.printf("<Setting> location for annotation processor root directories");
		flipJFM.setLocation(StandardLocation.ANNOTATION_PROCESSOR_PATH, annotationProcessorRootDirs);
		if (Murprum.isDebug()) S.printf("</Done>%n");
				
		if (Murprum.isDebug()) {
			S.printf("<Listing> possible annotation processors from StandardJavaFileManager");
			for (JavaFileObject javaFileObject : flipJFM.list(StandardLocation.ANNOTATION_PROCESSOR_PATH, "", EnumSet.of(Kind.CLASS), true)) {
				S.printf("- %s", javaFileObject.getName());
			}
			S.printf("</Done>%n");
		}
	}

	private boolean debugCompilationSuccessfull() throws IOException {
		if (!Murprum.isDebug()) return false;
		S.funcArgs();

		S.printf("#");
		S.printf("# Compilation successful.");
		S.printf("#%n");
		
		ArrayList<StandardLocation> standardLocations = new ArrayList<StandardLocation>();
//		standardLocations.add(StandardLocation.ANNOTATION_PROCESSOR_PATH);
		standardLocations.add(StandardLocation.CLASS_OUTPUT);
//		standardLocations.add(StandardLocation.CLASS_PATH);
//		standardLocations.add(StandardLocation.PLATFORM_CLASS_PATH);
		standardLocations.add(StandardLocation.SOURCE_OUTPUT);
//		standardLocations.add(StandardLocation.SOURCE_PATH);
		
		for (StandardLocation standardLocation : standardLocations) {
			S.printf("<Listing> outputted entries from StandardJavaFileManager, location %s", standardLocation.getName());

			Iterable<JavaFileObject> compileCreatedEntries;
			compileCreatedEntries = flipJFM.list(standardLocation, "", EnumSet.allOf(Kind.class), true);

			for (JavaFileObject javaFileObject : compileCreatedEntries) {
				S.printf("- %s", javaFileObject.getName());
			}
			S.printf("</Done>%n");
		}
		
		return true;
	}

	public FlipClassLoader getAnnotationProcessorClassLoader() {
		return (FlipClassLoader) flipJFM.getClassLoader(StandardLocation.ANNOTATION_PROCESSOR_PATH);
	}
	
	public FlipClassLoader getCompileOutputClassLoader() {
		return (FlipClassLoader) flipJFM.getClassLoader(StandardLocation.CLASS_OUTPUT);
	}
	
}
