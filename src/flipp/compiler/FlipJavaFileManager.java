/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.compiler;

import flipp.helper.FlipHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;

import murprum.Murprum;
import murprum.sys.S;

/**
 * 
 * 
 * @author Gima
 */
public class FlipJavaFileManager implements StandardJavaFileManager {
	
	/*
	 * An object of this (JavaFileManager) interface is not required to support multi-threaded access,
	 * that is, be synchronized. However, it (JavaFileManager) must support concurrent access to different
	 * file objects (JavaFileObject) created by this (JavaFileManager) object.
	 * Implementation note: a consequence of this requirement is that a trivial implementation of output to a
	 * JarOutputStream is not a sufficient implementation. That is, rather than creating a JavaFileObject that returns
	 * the JarOutputStream directly, the contents must be cached until closed and then written to the JarOutputStream.
	 * 
	 * Source: http://download.oracle.com/javase/6/docs/api/javax/tools/JavaFileManager.html
	 */
	
	private final StandardJavaFileManager forwardJFM;
	private final ConcurrentHashMap<Location, Boolean> handledLocations;
	private final ConcurrentHashMap<Location, LinkedList<FlipMemoryJavaFileObject>> handledJFOLocations;
	private final HashMap<Location, FlipClassLoader> flipClassLoaders;
	
	public FlipJavaFileManager(StandardJavaFileManager standardJavaFileManager) {
		forwardJFM = standardJavaFileManager;
		handledLocations = new ConcurrentHashMap<JavaFileManager.Location, Boolean>();
		handledJFOLocations = new ConcurrentHashMap<JavaFileManager.Location, LinkedList<FlipMemoryJavaFileObject>>();
		flipClassLoaders = new HashMap<JavaFileManager.Location, FlipClassLoader>();
		
		flipClassLoaders.put(StandardLocation.ANNOTATION_PROCESSOR_PATH, new FlipClassLoader(this, StandardLocation.ANNOTATION_PROCESSOR_PATH));
		flipClassLoaders.put(StandardLocation.CLASS_OUTPUT, new FlipClassLoader(this, StandardLocation.CLASS_OUTPUT));
	}

	@Override
	public ClassLoader getClassLoader(Location location) {
		FlipClassLoader flipClassLoaderForLocation = flipClassLoaders.get(location);
		if (flipClassLoaderForLocation != null) return flipClassLoaderForLocation;
		else return forwardJFM.getClassLoader(location);
	}

	@Override
	public Iterable<JavaFileObject> list(Location location, String packageName, Set<Kind> kinds, boolean recurse) throws IOException {
		if (!isHandledLocation(location)) {
			return forwardJFM.list(location, packageName, kinds, recurse);
		}
		
		LinkedList<FlipMemoryJavaFileObject> jfosInLocation = handledJFOLocations.get(location);
		if (jfosInLocation == null) {
			return new ArrayList<JavaFileObject>();
		}
		
		LinkedList<JavaFileObject> foundJFOs = new LinkedList<JavaFileObject>(); 
		
		for (FlipMemoryJavaFileObject flipJFO : jfosInLocation) {
			if (!kinds.contains(flipJFO.getKind())) continue;
			if (!(recurse && flipJFO.isUnderPackage(packageName))) continue;
			if (flipJFO.isInPackage(packageName)) continue;
			
			foundJFOs.add(flipJFO);
		}
		
		return foundJFOs;
	}

	@Override
	public String inferBinaryName(Location location, JavaFileObject javaFileObject) {
		if (!isHandledLocation(location)) return forwardJFM.inferBinaryName(location, javaFileObject);
		FlipMemoryJavaFileObject flipJFO = ((FlipMemoryJavaFileObject) javaFileObject);
		return S.sprintf(
				"%s/%s",
				flipJFO.getPackageName().replace('.', '/'),
				flipJFO.getRelativeName());
	}

	@Override
	public boolean handleOption(String current, Iterator<String> remaining) {
		return forwardJFM.handleOption(current, remaining);
	}

	@Override
	public boolean hasLocation(Location location) {
		return forwardJFM.hasLocation(location);
	}

	@Override
	public JavaFileObject getJavaFileForInput(Location location, String fullClassName, Kind kind) throws IOException {
		if (!isHandledLocation(location)) return forwardJFM.getJavaFileForInput(location, fullClassName, kind);
		
		String packageName = FlipHelper.getPackageName(fullClassName);
		String fileName = FlipHelper.getFileNameFromRelClassName(FlipHelper.getOnlyClassName(fullClassName), kind);
		
		FlipMemoryJavaFileObject foundJFO = findHandledJFO(location, packageName, fileName, kind);
		if (foundJFO != null) return foundJFO;
		else {
			FlipMemoryJavaFileObject newJFO = new FlipMemoryJavaFileObject(this, packageName, fileName, kind, location);
			putJFOToLocation(newJFO, location);
			return newJFO;
		}
	}
	
	@Override
	public JavaFileObject getJavaFileForOutput(Location location, String fullClassName, Kind kind, FileObject sibling) throws IOException {
		if (!isHandledLocation(location)) return forwardJFM.getJavaFileForOutput(location, fullClassName, kind, sibling);
		
		String packageName = FlipHelper.getPackageName(fullClassName);
		String fileName = FlipHelper.getFileNameFromRelClassName(FlipHelper.getOnlyClassName(fullClassName), kind);
		
		FlipMemoryJavaFileObject foundJFO = findHandledJFO(location, packageName, fileName, kind);
		if (foundJFO != null) return foundJFO;
		else {
			FlipMemoryJavaFileObject newJFO = new FlipMemoryJavaFileObject(this, packageName, fileName, kind, location);
			putJFOToLocation(newJFO, location);
			return newJFO;
		}
	}

	@Override
	public FileObject getFileForInput(Location location, String packageName, String relativeName) throws IOException {
		if (!isHandledLocation(location)) return forwardJFM.getFileForInput(location, packageName, relativeName);
		
		FlipMemoryJavaFileObject foundJFO = findHandledJFO(location, packageName, relativeName, Kind.OTHER);
		if (foundJFO != null) return foundJFO;
		else {
			FlipMemoryJavaFileObject newJFO = new FlipMemoryJavaFileObject(this, packageName, relativeName, Kind.OTHER, location);
			putJFOToLocation(newJFO, location);
			return newJFO;
		}
	}

	@Override
	public FileObject getFileForOutput(Location location, String packageName, String relativeName, FileObject sibling) throws IOException {
		if (!isHandledLocation(location)) return forwardJFM.getFileForOutput(location, packageName, relativeName, sibling);
		
		FlipMemoryJavaFileObject foundJFO = findHandledJFO(location, packageName, relativeName, Kind.OTHER);
		if (foundJFO != null) return foundJFO;
		else {
			FlipMemoryJavaFileObject newJFO = new FlipMemoryJavaFileObject(this, packageName, relativeName, Kind.OTHER, location);
			putJFOToLocation(newJFO, location);
			return newJFO;
		}
	}

	@Override
	public void flush() throws IOException {
		forwardJFM.flush();
	}

	@Override
	public void close() throws IOException {
		forwardJFM.close();
	}

	@Override
	public int isSupportedOption(String option) {
		return forwardJFM.isSupportedOption(option);
	}

	@Override
	public boolean isSameFile(FileObject a, FileObject b) {
		if ((!(a instanceof FlipMemoryJavaFileObject)) || (!(b instanceof JavaFileObject)) ) return forwardJFM.isSameFile(a, b);
		
		FlipMemoryJavaFileObject aJFO = (FlipMemoryJavaFileObject) a;
		FlipMemoryJavaFileObject bJFO = (FlipMemoryJavaFileObject) b;
		return aJFO.equals(bJFO);
	}

	@Override
	public Iterable<? extends JavaFileObject> getJavaFileObjectsFromFiles(Iterable<? extends File> files) {
		return forwardJFM.getJavaFileObjectsFromFiles(files);
	}

	@Override
	public Iterable<? extends JavaFileObject> getJavaFileObjects(File... files) {
		return forwardJFM.getJavaFileObjects(files);
	}

	@Override
	public Iterable<? extends JavaFileObject> getJavaFileObjectsFromStrings(Iterable<String> names) {
		return forwardJFM.getJavaFileObjectsFromStrings(names);
	}

	@Override
	public Iterable<? extends JavaFileObject> getJavaFileObjects(String... names) {
		return forwardJFM.getJavaFileObjects(names);
	}

	@Override
	public void setLocation(Location location, Iterable<? extends File> path) throws IOException {
		forwardJFM.setLocation(location, path);
	}
	
	@Override
	public Iterable<? extends File> getLocation(Location location) {
		return forwardJFM.getLocation(location);
	}
	
	public boolean isHandledLocation(Location location) {
		Boolean inMemory = handledLocations.get(location);
		if (inMemory == null) return false;
		else if (inMemory == Boolean.FALSE) return false;
		else return true;
	}
	
	public void setLocationHandled(Location location, boolean b) {
		if (b == true) handledLocations.put(location, Boolean.TRUE);
		else handledLocations.put(location, Boolean.FALSE);
	}
	
	public boolean deleteJFO(Location location, FlipMemoryJavaFileObject flipJFO) {
		FlipMemoryJavaFileObject foundJFO = findHandledJFO(location, flipJFO.getPackageName(), flipJFO.getRelativeName(), flipJFO.getKind());
		if (foundJFO == null) return false;
		
		return handledJFOLocations.get(location).remove(foundJFO);
	}
	
	public FlipMemoryJavaFileObject findHandledClassJFO(String fullClassName) {
		String packageName = FlipHelper.getPackageName(fullClassName);
		String className = FlipHelper.getOnlyClassName(fullClassName);
		return findHandledJFO(StandardLocation.CLASS_OUTPUT, packageName, className + ".class", Kind.CLASS);
	}
	
	private void putJFOToLocation(FlipMemoryJavaFileObject newJFO, Location location) {
		LinkedList<FlipMemoryJavaFileObject> handledJFOs = handledJFOLocations.get(location);
		if (handledJFOs == null) {
			handledJFOs = new LinkedList<FlipMemoryJavaFileObject>();
			handledJFOLocations.put(location, handledJFOs);
		}
		handledJFOs.add(newJFO);
	}

	private FlipMemoryJavaFileObject findHandledJFO(Location location, String packageName, String relativeName, Kind kind) {
		if (Murprum.isDebug()) S.funcArgs(location, packageName, relativeName, kind.name());
		LinkedList<FlipMemoryJavaFileObject> handledJFOs = handledJFOLocations.get(location);
		if (handledJFOs == null) return null;
		
		for (FlipMemoryJavaFileObject flipJFO : handledJFOs) {
			if (!flipJFO.isInPackage(packageName)) continue;
			if (!flipJFO.getRelativeName().equals(relativeName)) continue;
			if (flipJFO.getKind() != kind) continue;
			
			return flipJFO;
		}
		
		return null;
	}
	
}