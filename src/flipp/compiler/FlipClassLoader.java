/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.compiler;

import flipp.helper.FlipHelper;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;

import javax.tools.JavaFileManager.Location;

import murprum.Murprum;
import murprum.stream.memory.BAInputStream;
import murprum.sys.S;

/**
 * 
 * 
 * @author Gima
 */
public class FlipClassLoader extends URLClassLoader {
	
	private final FlipJavaFileManager containingJFM;
	private final Location forLocation;
	private final LinkedList<File> nativeLibraryDirs;
	
	public FlipClassLoader(FlipJavaFileManager containingJFM, Location forLocation) {
		super(new URL[] {});
		this.containingJFM = containingJFM;
		this.forLocation = forLocation;
		nativeLibraryDirs = new LinkedList<File>();
	}
	
	@Override
	public Class<?> findClass(String className) throws ClassNotFoundException {
		if (forLocation == null) {
			if (Murprum.isDebug()) S.funcArgs(className, "(Used without JavaFileManager. Maybe by Launcher)", hashCode());
		}
		else {
			if (Murprum.isDebug()) S.funcArgs(className, forLocation.getName(), hashCode());
		}
		
		Class<?> foundClass = null;
		
		try {
			foundClass = super.findClass(className);
			if (Murprum.isDebug()) S.debug("URLClassLoader found the class: %s", className);
			return foundClass;
		}
		catch (ClassNotFoundException e) {
			if (Murprum.isDebug()) S.debug("URLClassLoader didn't find the class: %s", className);
		}

		foundClass = tryToLoadFromMemory(className);
		if (foundClass == null) {
			if (Murprum.isDebug()) S.debug("FlipClassLoader didn't find the class (from memory): %s", className);
			throw new ClassNotFoundException(className);
		}
		else {
			if (Murprum.isDebug()) S.debug("FlipClassLoader found the class (from memory): %s", className);
			return foundClass;
		}
	}
	
	@Override
	public void addURL(URL url) {
		if (Murprum.isDebug()) S.funcArgs(url, hashCode());
		super.addURL(url);
	}
	
	@Override
	public String findLibrary(String libname) {
		if (Murprum.isDebug()) S.funcArgs(libname);
		System.out.println("System.getProperty(\"os.arch\") == " + System.getProperty("os.arch"));
		String libPath = super.findLibrary(libname);
		if (libPath != null) return libPath;
		
		for (File nativeLibRootDir : nativeLibraryDirs) {
			String libSuffix = getOSSharedLibSuffix();
			if (libSuffix == null) return null;
			File tempLib = new File(nativeLibRootDir, libname + libSuffix);
			if (Murprum.isDebug()) S.printf("- Finding %s", tempLib.toString());
			if (tempLib.exists()) return tempLib.getAbsoluteFile().toString();
		}
		
		return null;
	}

	private String getOSSharedLibSuffix() {
		String osName = System.getProperty("os.name");
		if (osName.toLowerCase().contains("bsd")) {
			return ".so";
		}
		else if (osName.toLowerCase().startsWith("linux")) {
			return ".so";
		}
		else if (osName.toLowerCase().startsWith("mac")) {
			return ".dylib";
			// and/or ".oslib"? "lib"-prefix?
			// return array to check for multiple files with different suffixes
		}
		else if (osName.toLowerCase().startsWith("win")) {
			return ".dll";
		}
		else {
			new Exception(S.sprintf(
					"Launcher doesn't know what shared library extension this operating system (%s) uses",
					System.getProperty("os.name"))).printStackTrace();
			return null;
		}
	}

	public void addNativeLibRootDir(File nativeLibraryDirFile) {
		nativeLibraryDirs.add(new File(nativeLibraryDirFile, FlipHelper.replaceOSPathVariables("${longos}-${arch}")));
		nativeLibraryDirs.add(new File(nativeLibraryDirFile, FlipHelper.replaceOSPathVariables("${shortos}-${arch}")));
		nativeLibraryDirs.add(new File(nativeLibraryDirFile, FlipHelper.replaceOSPathVariables("${longos}")));
		nativeLibraryDirs.add(new File(nativeLibraryDirFile, FlipHelper.replaceOSPathVariables("${shortos}")));
		nativeLibraryDirs.add(new File(nativeLibraryDirFile, FlipHelper.replaceOSPathVariables("${arch}")));
		nativeLibraryDirs.add(nativeLibraryDirFile);
	}
	
	public LinkedList<File> getNativeLibraryDirs() {
		return nativeLibraryDirs;
	}
	
	private Class<?> tryToLoadFromMemory(String fullClassName) {
		if (containingJFM == null) return null;
		if (!containingJFM.isHandledLocation(forLocation)) return null;
		
		FlipMemoryJavaFileObject foundJFO = containingJFM.findHandledClassJFO(fullClassName);
		if (foundJFO == null) return null;
		
		BAInputStream classInputStream = (BAInputStream) foundJFO.openInputStream();
		byte[] classBytes = classInputStream.copyBytes();
		return defineClass(fullClassName, classBytes, 0, classBytes.length);
	}
	
}