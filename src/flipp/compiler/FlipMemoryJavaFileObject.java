/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.compiler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.NestingKind;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;

import murprum.Murprum;
import murprum.stream.memory.BAInputStream;
import murprum.stream.memory.BAOutputStream;
import murprum.stream.memory.BAStream;
import murprum.sys.S;

/**
 * 
 * 
 * @author Gima
 */
class FlipMemoryJavaFileObject implements JavaFileObject {
	
	private final Location location;
	private final String packageName;
	private final String relativeName;
	private final Kind kind;
	private final URI uri;
	private final FlipJavaFileManager containingJFM;
	private final BAStream baStream;
	
	public FlipMemoryJavaFileObject(FlipJavaFileManager containingJFM, String packageName, String relativeName, Kind kind, Location location) throws IOException {
		if (Murprum.isDebug()) S.funcArgs(containingJFM, packageName, relativeName, kind.name(), location.getName());
		this.packageName = packageName;
		this.relativeName = relativeName;
		this.kind = kind;
		this.location = location;
		String fullName;
		if (packageName.length() == 0) fullName = relativeName;
		else fullName = packageName + "/" + relativeName;
		
		try {
			uri = new URI("memory", "", "/" + fullName, null, null);
		}
		catch (URISyntaxException e) {
			throw new IOException(S.sprintf("URI syntax contruction failed(%s) for: %s", e.getMessage(), fullName));
		}		
		
		if (Murprum.isDebug()) {
			S.debug("new JavaFileObject(packageName:%s, relativeName:%s, kind:%s, location:%s, uri:%s)", packageName, relativeName, kind, location, uri);
		}
		
		this.containingJFM = containingJFM;
		
		baStream = new BAStream(1024*512);
	}

	public boolean isInPackage(String packageName) {
		return this.packageName.equals(packageName);
	}

	public boolean isUnderPackage(String packageName) {
		return this.packageName.startsWith(packageName);
	}

	public Location getLocation() {
		return location;
	}
	
	public String getPackageName() {
		return packageName;
	}

	public String getRelativeName() {
		return relativeName;
	}
	
	@Override
	public URI toUri() {
		return uri;
	}
	
	@Override
	public String getName() {
		return S.sprintf(
				"%s%s",
				packageName,
				relativeName);
	}

	@Override
	public InputStream openInputStream() {
		return new BAInputStream(baStream);
	}

	@Override
	public OutputStream openOutputStream() {
		return new BAOutputStream(baStream);
	}

	@Override
	public Reader openReader(boolean ignoreEncodingErrors) throws IOException {
		return new InputStreamReader(new BAInputStream(baStream));
	}
	
	public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
		BAInputStream baInputStream = new BAInputStream(baStream);
		
		return new String(baInputStream.copyBytes());
	}

	@Override
	public Writer openWriter() throws IOException {
		return new OutputStreamWriter(new BAOutputStream(baStream));
	}

	@Override
	public boolean delete() {
		return containingJFM.deleteJFO(location, this);
	}

	@Override
	public Kind getKind() {
		return kind;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof FlipMemoryJavaFileObject)) return super.equals(obj);
		FlipMemoryJavaFileObject otherJFO = (FlipMemoryJavaFileObject) obj;

		return toUri().equals(otherJFO.toUri()) && (getLocation() == otherJFO.getLocation()) && (getKind() == otherJFO.getKind());
	}

	@Override
	public boolean isNameCompatible(String otherSimpleName, Kind othersKind) {
		String simpleName = relativeName.replaceFirst("^([^\\.]*)\\..*$", "$1");
		return (simpleName.equals(otherSimpleName) && (getKind() == othersKind));
	}

	@Override
	public long getLastModified() {
		// docu says: returns 0 if the operation is not supported (among other possibilities)
		return 0;
	}

	@Override
	public NestingKind getNestingKind() {
		// matters? docu says it doesn't
		return null;
	}

	@Override
	public Modifier getAccessLevel() {
		// matters? docu says it doesn't
		return null;
	}

}