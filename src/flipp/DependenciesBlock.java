/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import murprum.sys.S;

/**
 * Data container for information found in Flip dependencies block.
 * 
 * @author Gima
 */
public class DependenciesBlock {
	
	private final HashMap<String, ArrayList<String>> compileEntryMap;
	private final LinkedList<String> classEntries;
	private final LinkedList<String> resourceEntries;
	private final HashMap<String, ArrayList<String>> nativeEntryMap;

	private static final Pattern compileEntryPattern;
	private static final Pattern nativeEntryPattern;
	
	/**
	 * Create an instance with no dependency entries
	 */
	public DependenciesBlock() {
		compileEntryMap = new HashMap<String, ArrayList<String>>();
		classEntries = new LinkedList<String>();
		resourceEntries = new LinkedList<String>();
		nativeEntryMap = new HashMap<String, ArrayList<String>>();
	}

	static {
		compileEntryPattern = Pattern.compile("^compile \"([^\"]+)\"$", Pattern.CASE_INSENSITIVE);
		nativeEntryPattern = Pattern.compile("^native \"([^\"]+)\"$", Pattern.CASE_INSENSITIVE);
	}
	

	/**
	 * Parse inputStream for Flip dependencies block and return an instance of this class populated with the found values.
	 * @return null if no block was found.
	 */
	public static DependenciesBlock parse(InputStream inputStream) throws IOException  {
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		boolean dependenciesBlockFound = false;
		boolean inDependenciesBlock = false;
		DependenciesBlock dependenciesBlock = new DependenciesBlock();
		SectionType blockType = null;
		String readLine;
		String compileEntryParamater = null;
		String nativeEntryParameter = null;
		
		while ((readLine = bufferedReader.readLine()) != null) {
			
			Matcher compileEntryMatcher = compileEntryPattern.matcher(readLine);
			Matcher nativeEntryMatcher = nativeEntryPattern.matcher(readLine);
			
			if ("/* FLIP DEPENDENCIES:".equals(readLine)) {
				inDependenciesBlock = true;
				dependenciesBlockFound = true;
			}
			else if (compileEntryMatcher.matches()) {
				compileEntryParamater = compileEntryMatcher.group(1);
				blockType = SectionType.COMPILE;
				continue;
			}
			else if ("classes".equals(readLine)) {
				blockType = SectionType.CLASSES;
				continue;
			}
			else if ("resources".equals(readLine)) {
				blockType = SectionType.RESOURCES;
				continue;
			}
			else if (nativeEntryMatcher.matches()) {
				blockType = SectionType.NATIVE;
				nativeEntryParameter = nativeEntryMatcher.group(1);
				dependenciesBlock.nativeEntryMap.put(nativeEntryParameter, new ArrayList<String>());
				continue;
			}
			else if ("*/".equals(readLine)) {
				inDependenciesBlock = false;
				continue;
			}
			
			if (!inDependenciesBlock) continue;
			if (blockType == null) continue;
			
			String currentEntryValue = readLine.trim();
			if (currentEntryValue.length() == 0) continue;
			
			switch (blockType) {
			case COMPILE:
				ArrayList<String> compileEntries = dependenciesBlock.compileEntryMap.get(compileEntryParamater);
				if (compileEntries == null) {
					compileEntries = new ArrayList<String>();
					dependenciesBlock.compileEntryMap.put(compileEntryParamater, compileEntries);
				}
				compileEntries.add(currentEntryValue);
				break;
				
			case CLASSES:
				dependenciesBlock.classEntries.add(currentEntryValue);
				break;

			case RESOURCES:
				dependenciesBlock.resourceEntries.add(currentEntryValue);
				break;

			case NATIVE:
				dependenciesBlock.nativeEntryMap.get(nativeEntryParameter).add(currentEntryValue);
				break;
			}

		} // while
		
		if (!dependenciesBlockFound) return null;
		return dependenciesBlock;
	}
	
	/**
	 * Mapped like so: key=directory root, value=fully qualified class path
	 */
	public HashMap<String, ArrayList<String>> getCompileEntryMap() {
		return compileEntryMap;
	}
	
	/**
	 * directories and jar files
	 */
	public LinkedList<String> getClassEntries() {
		return classEntries;
	}
	
	/**
	 * root directories for resources
	 */
	public LinkedList<String> getResourceEntries() {
		return resourceEntries;
	}
	
	/**
	 * funny map
	 */
	public HashMap<String, ArrayList<String>> getNativeEntries() {
		return nativeEntryMap;
	}
	
	public void dumpEntries() {
		
		S.printf("<Listing> 'compile'. %d root directories:", compileEntryMap.size());
		for (String key : compileEntryMap.keySet()) {
			ArrayList<String> compileEntries = compileEntryMap.get(key);
			S.printf("[%s (%d)]:", key, compileEntries.size());
			for (String entryValue : compileEntries) {
				S.printf(" - %s", entryValue);
			}
		}
		S.printf("</Listing>%n");
		
		S.printf("<Listing> 'classes' dependencies (%d):", classEntries.size());
		for (String classEntry : classEntries) {
			S.printf("- %s", classEntry);
		}
		S.printf("</Listing>%n");
		
		S.printf("<Listing> 'resources' dependencies (%d):", resourceEntries.size());
		for (String resourceEntry : resourceEntries) {
			S.printf("- %s", resourceEntry);
		}
		S.printf("</Listing>%n");
		
		S.printf("<Listing> 'native' dependencies:");
		for (String nativeDir : nativeEntryMap.keySet()) {
			S.printf("[%s]", nativeDir);
			for (String osIdentifier : nativeEntryMap.get(nativeDir)) {
				S.printf(" - %s", osIdentifier);
			}
		}
		S.printf("</Listing>%n");
	}
	
	/**
	 * Specifies which kind of dependency block entry is in question.
	 * 
	 * @author Gima
	 */
	private static enum SectionType {
		COMPILE,
		CLASSES,
		RESOURCES,
		NATIVE,
	}

}
