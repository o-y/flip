/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

import murprum.Murprum;
import murprum.sys.S;
import flipp.launcher.Launcher;
import flipp.project.FlipMemoryProject;

/**
 * Flip can run one .java file.<br>
 * This file will be referred to as the "entry" program. And because a Java program can have dependencies, Flip can read a
 * specially crafted comment block, where the dependencies are specified. This comment block will be referred to as the
 * "dependencies block" and it can reside anywhere in the entry program's source file.
 * <p>
 * The dependencies block can include the following sections:
 * <p>
 * 
 * <code>compile "root directory"</code>
 * <blockquote>
 * Specifies which other .java files the entry program depends on. Entries are fully qualified class names.
 * The "root directory" specifies the root directory for all the classes defined this compile block
 * and is relative to Flip's SRCBASE command line argument, unless absolute.
 * 
 * </blockquote>
 * 
 * <code>classes</code>
 * <blockquote>
 * Specifies which .class and .jar files that need to be included in the classpath before entry program's compilation and running.
 * The entries are paths relative to current working directory, unless absolute.
 * Entries can be .jar files or root directories of classes.
 * </blockquote>
 * 
 * <code>resources</code>
 * <blockquote>
 * Specifies which resources will be available to the entry program.
 * The entries are paths relative to current working directory, unless absolute.
 * The paths can point to resource root directories or to .jar files.
 * </blockquote>
 * 
 * <code>native</code>
 * <blockquote>
 * Specifies which directories will be searched for native libraries loaded in the entry program.
 * The entries are paths relative to current working directory, unless absolute.
 * <p>
 * Native libraries will be searched in the following way:<br>
 * 1. <code>[dir]/{@link System#getProperty(String) System.getProperty("os.name")}-{@link System#getProperty(String) System.getProperty("os.arch")}/</code><br>
 * 2. <code>[dir]/{@link System#getProperty(String) System.getProperty("os.name")}</code>(simplified)<code>-{@link System#getProperty(String) System.getProperty("os.arch")}/</code><br>
 * 3. <code>[dir]/{@link System#getProperty(String) System.getProperty("os.name")}/</code><br>
 * 4. <code>[dir]/{@link System#getProperty(String) System.getProperty("os.name")}</code>(simplified)<code>/</code>
 * 5. <code>[dir]/{@link System#getProperty(String) System.getProperty("os.arch")}</code>
 * 6. <code>[dir]/</code>
 * <p>
 * The search order for Windows 7 64bit would be:<br>
 * 1. [dir]/Windows 7-amd64/<br>
 * 2. [dir]/win-amd64/<br>
 * 3. [dir]/Windows 7/<br>
 * 4. [dir]/win/<br>
 * 5. [dir]/amd64/<br>
 * 6. [dir]/<br>
 * </blockquote>
 * 
 * <p>
 * An example of this specially formed comment block follows:
 * <blockquote><code><pre>
 * /* FLIP DEPENDENCIES:
 * compile "root_compiletest"
 * 	compiletestpkg.TestCompileDep
 * 	
 * classes
 * 	root_classtest/classtestpkg/
 * 	root_jartest/pkg.jar
 * 	
 * compile "root_resourcetest"
 * 	resourcetestpkg.TestResourceDep
 * 	
 * resources
 * 	root_resourcestest/
 * 	
 * compile "root_nativetest"
 * 	nativetestpkg.TestNativeDep
 * 	
 * native
 * 	nativetest_libs/${shortos}-${arch}/
 * 	nativetest_libs/${shortos}-${arch}/
 * 	nativetest_libs/${shortos}/
 * *&#47;</pre></code></blockquote>
 * 
 * @author Gima
 */
public class Flip {

	static FlipMemoryProject flipProject;
	
	static File entryClassRootDir;
	static String entryClassName;
	static File entryClassSourceFile;
	static DependenciesBlock dependenciesBlock;

	public static void main(String[] args) throws Throwable {
		if (new File("META-INF").isDirectory()) {
			Launcher.executeLauncher(args);
			return;
		}
		
		// handle debug argument
		if (args.length > 1) {
			if (args[0].equals("debug")) {
				Murprum.setDebug(true);
				// if debug argument was given to Flip strip it from args
				args = Arrays.copyOfRange(args, 1, args.length);
			}
			else Murprum.setDebug(false);
		}
		
		if (args.length < 2) {
			S.eprintfn(
					"#%n" +
					"# Flip compiles and runs a .java file in memory.%n" +
					"#%n" +
					"%n" +
					"Usage: flip [debug] <SRCBASE> <CLASS> [arguments..]%n" +
					"%n" +
					"  debug      =  Enables debugging output.%n" +
					"               Be wary, it's messy.%n" +
					"%n" +
					"  SRCBASE    =  Root directory for the java source file.%n" +
					" %n" +
					"  CLASS      =  Fully qualified class name to run (.java source file).%n" +
					"                Relative to the SRCBASE.%n" +
					"%n" +
					"  arguments  =  Arguments passed to CLASS's main method.%n" +
					"%n" +
					"Examples:%n" +
					"  flip . build%n" +
					"  flip src com.example.HelloWorld%n");
			return;
		}

		flipProject = new FlipMemoryProject();
		
		entryClassRootDir = getEntryClassRootDir(args);
		entryClassName = args[1];
		entryClassSourceFile = new File(entryClassRootDir, entryClassName.replace('.', '/') + ".java");

		dependenciesBlock = getDependenciesBlock(entryClassSourceFile);

		debugDumpStartingVariables();

		
		flipProject.getSettings().addSrcRootDir(entryClassRootDir.toString());
		
		// add source root directories to FlipProject (as specified in compile dependencies block)
		for (String srcRootDir : dependenciesBlock.getCompileEntryMap().keySet()) {
			
			File srcRootDirFile = getAbsoluteDir(srcRootDir);
			if (srcRootDirFile == null) return;

			flipProject.getSettings().addSrcRootDir(srcRootDirFile.toString());
		}

		
		flipProject.getSettings().addClassToCompile(entryClassName);

		// add class names to be compiled (as specified in compile dependencies block)
		for (ArrayList<String> sourceClassNameCollections : dependenciesBlock.getCompileEntryMap().values()) {
			for (String sourceClassName : sourceClassNameCollections) {
				flipProject.getSettings().addClassToCompile(sourceClassName);
			}
		}
		
		for (String libEntry : dependenciesBlock.getClassEntries()) {
			File libEntryFile = new File(libEntry).getAbsoluteFile();
			if (libEntryFile == null) return;
			
			flipProject.getSettings().addClassPathEntry(libEntryFile.toString());
		}
		
		for (String resourceEntry : dependenciesBlock.getResourceEntries()) {
			File resourceEntryFile = new File(resourceEntry).getAbsoluteFile();
			if (resourceEntryFile == null) return;
			
			flipProject.getSettings().addResource(resourceEntryFile.toString());
		}
		
		for (String dirEntry : dependenciesBlock.getNativeEntries().keySet()) {
			for (String osIdentifier : dependenciesBlock.getNativeEntries().get(dirEntry)) {
				flipProject.getSettings().addNativeLibDir(dirEntry, osIdentifier);
			}
		}
		
		flipProject.getSettings().setCompilerOptions("-Xprefer:newer", "-Xlint", "-encoding", "UTF-8");


		// compile
		if (Murprum.isDebug()) S.debug("About to compile%n");
		flipProject.compile();
		
		
		if (Murprum.isDebug()) S.debug("About to run entry class%n");
		
		// extract rest of Flip's arguments and pass them to entry class
		String[] passArgs = getPassArgs(args);
		
		try {
			flipProject.runMain(entryClassName, passArgs);
		}
		catch (InvocationTargetException e) {
			throw e.getCause();
		}
		
		if (Murprum.isDebug()) S.debug("Returned from entry class%n");

	} // main

	private static String[] getPassArgs(String[] args) {
		if (args.length > 2) {
			return Arrays.copyOfRange(args, 2, args.length-1); 
		}
		else {
			return new String[] {};
		}
	}
	
	/**
	 * Returns absolute File, unless directory is already absolute, in which case File of directory is returned.
	 * @return null if failed
	 * @throws IOException 
	 */
	private static File getAbsoluteDir(String dir) throws IOException {
		File dirFile = new File(dir);
		if (dirFile.isAbsolute()) return dirFile;
		else {
			return new File(dir).getCanonicalFile().getAbsoluteFile();
		}
	}

	private static void debugDumpStartingVariables() {
		if (!Murprum.isDebug()) return;

		S.printf("<Listing> Flip starting variables>");
		S.printf("ClassPath: %s", System.getProperty("java.class.path"));
		S.printf("user.dir: %s", System.getProperty("user.dir"));
		S.printf("sourceRootDir: %s", entryClassRootDir);
		S.printf("entryClassName: %s", entryClassName);
		S.printf("sourceFile: %s", entryClassSourceFile.toString());
		S.printf("</Listing>%n");
		
		dependenciesBlock.dumpEntries();
	}

	private static File getEntryClassRootDir(String[] args) throws IOException {
		File ecbd = new File(args[0]).getAbsoluteFile();
		if (!ecbd.isDirectory()) {
			S.eprintf("Entry class root directory is not a directory: %s", ecbd.toString());
			return null;
		}
		return ecbd;
	}

	private static DependenciesBlock getDependenciesBlock(File sourceFile) throws IOException {
		InputStream sourceInputStream = null;

		try {
			sourceInputStream = new FileInputStream(sourceFile);
			
			DependenciesBlock dependenciesBlock = DependenciesBlock.parse(sourceInputStream);
			if (dependenciesBlock == null) {
				if (Murprum.isDebug()) S.printf("Dependencies block was not found. Creating empty dependency container");
				return new DependenciesBlock();
			}
			else return dependenciesBlock;
			
		}
		finally {
			if (sourceInputStream != null) {
				try { sourceInputStream.close(); } catch (IOException e) {}
			}
		}
	}
	
}
