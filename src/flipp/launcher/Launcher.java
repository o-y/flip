/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.launcher;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import murprum.Murprum;
import murprum.java.ClassHelper;
import murprum.sys.S;
import flipp.compiler.FlipClassLoader;
import flipp.exceptions.MainMethodNotFoundException;

public class Launcher {

	public static void executeLauncher(String[] args) throws Throwable {
		
		// handle debug argument
		if (args.length > 0) {
			if (args[0].equals("debug")) {
				Murprum.setDebug(true);
				// if debug argument was given to Flip strip it from args
				args = Arrays.copyOfRange(args, 1, args.length);
			}
			else Murprum.setDebug(false);
		}
		
		if (Murprum.isDebug()) S.funcArgs();
		FlipClassLoader flipClassLoader = new FlipClassLoader(null, null);

		// add bin/ directory as class files search directory
		flipClassLoader.addURL(new File("bin").toURI().toURL());
		
		// add lib/ directory as native libraries search algorithm root dir
		flipClassLoader.addNativeLibRootDir(new File("native"));

		// add all jar files in lib/ directory to classloader
		// for the moment ignore dirs even though they are allowed to be specified as classpath entries
		// in FlipProject
		File libDir = new File("lib");
		if (libDir.isDirectory()) {
			for (File libFile : libDir.listFiles()) {
				if (libFile.isDirectory()) continue;
				if (libFile.toString().endsWith(".jar")) {
					flipClassLoader.addURL(libFile.toURI().toURL());
				}
			}
		}
		
		// add every directory in resources as a possible resource root directory
		File resDir = new File("res");
		if (resDir.isDirectory()) {
			for (File resRootDir : resDir.listFiles()) {
				if (!resRootDir.isDirectory()) continue;
				flipClassLoader.addURL(resRootDir.toURI().toURL());
			}
		}
		
		// load main class name from manifest
		FileInputStream manifestFileInStream = new FileInputStream(new File("META-INF", "MANIFEST.MF"));
		Manifest manifest = new Manifest(manifestFileInStream);
		Attributes attributes = manifest.getMainAttributes();
		String mainClassName = attributes.getValue(Attributes.Name.MAIN_CLASS);
		if (mainClassName == null) {
			throw new Exception("Couldn't find Main-Class from MANIFEST.MF");
		}
		manifestFileInStream.close();
		
		// load main class
		Class<?> mainClass = flipClassLoader.loadClass(mainClassName);
		
		// find main method
		Method mainMethod = ClassHelper.getMainMethod(mainClass);
		if (mainMethod == null) {
			throw new MainMethodNotFoundException(S.sprintf("Main method not found for class %s", mainClassName));
		}
		
		if (Murprum.isDebug()) {
			S.printf("<Listing> native libraries in FlipClassLoader");
			for (File dir : flipClassLoader.getNativeLibraryDirs()) {
				S.printf("- %s", dir);
			}
			S.printf("</Done>%n");
		}
		
		// execute main method with arguments given to this class constructor
		try {
			mainMethod.invoke(mainClass, (Object)args);
		}
		catch (InvocationTargetException e) {
			throw e.getCause();
		}
	}

}
