/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.helper;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Properties;

import murprum.file.FileHelper;
import murprum.sys.S;

public class FlipConfig {
	
	private static Properties configEntries;

	static {
		configEntries = new Properties();
		try {
			if (new File(FileHelper.getCodeRoot()).isFile()) {
				configEntries.load(new FileInputStream(new File(FileHelper.getCodeRootDir(), "flip.properties")));
			}
			else if (new File(FileHelper.getCodeRoot()).isDirectory()) {
				// assume configuration file is below bin/ and we are run from within /bin
				configEntries.load(new FileInputStream(new File(FileHelper.getCodeRootDir().getParentFile(), "flip.properties")));
			}
			else {
				throw new Exception(S.sprintf(
						"Could not load configuration file; Not run from .jar or .class file. Run from: %s",
						FileHelper.getCodeRoot().toString()
						));
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * @return null if no entry found
	 */
	public static String getConfigEntry(String key) {
		return configEntries.getProperty(key);
	}
	
	/**
	 * For given libName, retrieve a jar entry.
	 * <p>
	 * jar.[libName]
	 * @return null if no entry found
	 */
	public static String getJarEntry(String libName) {
		return getConfigEntry("jar." + libName);
	}
	
	/**
	 * For given libName, retrieve all matching native entries from config.
	 * <p>
	 * native.[libName].[osIdentifier] 
	 * @return Empty map if no native entries found.
	 */
	public static LinkedHashMap<String, LinkedList<String>> getLibEntries(String libName) {
		
		LinkedHashMap<String, LinkedList<String>> nativeLibMap = new LinkedHashMap<String, LinkedList<String>>();
		
		// loop config entries
		for (Entry<Object, Object> entry : configEntries.entrySet()) {
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();
			
			// match value native library and to given libName
			if (!key.startsWith("native." + libName)) continue;
			String osIdentifier = key.replace("native." + libName + ".", "").replace('_', ' ');
			
			// create list of nativeLibs under osIdentifier in the Map if it didn't exist
			LinkedList<String> nativeLibDirs = nativeLibMap.get(osIdentifier);
			if (nativeLibDirs == null) {
				nativeLibDirs = new LinkedList<String>();
				nativeLibMap.put(osIdentifier, nativeLibDirs);
			}
			
			// add entry to nativeLibMap under osIdentifier
			nativeLibDirs.add(value);
		}
		
		return nativeLibMap;
	}
	
}
