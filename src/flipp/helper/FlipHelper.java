/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package flipp.helper;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.tools.JavaFileObject.Kind;

import murprum.Murprum;
import murprum.file.FileHelper;
import murprum.sys.S;

public class FlipHelper {
	
	public static String getOnlyClassName(String fullClassName) {
		String className = fullClassName.replaceFirst("^.*\\.([^\\.]*)$", "$1");
		return className;
	}

	public static String getPackageName(String fullClassName) {
		String tmpPkgName = fullClassName.replaceFirst("^(.*)\\.[^\\.]*$", "$1");
		if (tmpPkgName.equals(fullClassName)) return "";
		else return tmpPkgName;
	}
	
	public static String getFileNameFromRelClassName(String relativeClassName, Kind kind) {
		return relativeClassName + kind.extension;
	}
	
	public static List<String> getClassesFrom(String dir) {
		if (Murprum.isDebug()) S.funcArgs(dir);
		LinkedList<String> classes = new LinkedList<String>();
		
		for (File file : FileHelper.listFiles(new File(dir).getAbsoluteFile())) {
			String relativeFile = new File(dir).getAbsoluteFile().toURI().relativize(file.getAbsoluteFile().toURI()).toString();
			if (!relativeFile.endsWith(".java")) continue;
			String className = relativeFile.replace('/', '.').replaceFirst("\\.java$", "");
			classes.add(className);
		}
		
		if (Murprum.isDebug()) {
			S.printf("<Listing> classes");
			for (String className : classes) {
				S.printf("- %s", className);
			}
			S.printf("</Done>%n");
		}
		
		return classes;
	}
	
	public static String replaceOSPathVariables(String entry) {
		
		// handle ${shortos}
		String osName = System.getProperty("os.name");
		String shortOSValue = null;
		
		if (osName.toLowerCase().contains("bsd")) {
			shortOSValue = "bsd";
		}
		else if (osName.toLowerCase().startsWith("linux")) {
			shortOSValue = "linux";
		}
		else if (osName.toLowerCase().startsWith("mac")) {
			shortOSValue = "mac";
		}
		else if (osName.toLowerCase().startsWith("win")) {
			shortOSValue = "win";
		}
		else {
			new Exception(S.sprintf(
					"Cannot expand ${shortos}; couldn't identify operating system (%s)",
					System.getProperty("os.name")
					));
		}
		
		if (shortOSValue != null) {
			entry = entry.replace("${shortos}", shortOSValue);
		}

		// handle ${longos}
		String longOSValue = System.getProperty("os.name");
		entry = entry.replace("${longos}", longOSValue);
		
		// handle ${arch}
		String archValue = System.getProperty("os.arch");
		entry = entry.replace("${arch}", archValue);
		

		// return (replaced) string
		return entry;
	}

}
