/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package nativetestpkg;

/**
 * 
 * 
 * @author Gima
 */
public class TestNativeDep {
	public static void test() {
		try {
			int returnedValue = NativeLib.getValue();
			if (returnedValue == 123) {
				System.out.println("Test 6/6 successful (TestNativeDep)");
			}
			else {
				System.out.println("Test 6/6 FAILED (TestNativeDep)");
				System.out.println(String.format(
					"  ..eh, what? The returned value should be 123, but we got: %d. So it \"kind of\" works?",
					returnedValue));
			}
		}
		catch (UnsatisfiedLinkError e) {
			System.out.println("Test 6/6 FAILED (TestNativeDep)");
			System.out.println("  [REASON]: \"" + e.getMessage() + "\", but:");
			System.out.println("  The native library 'NativeLib.xyz' is not available for any OS, yet. Yay!");
			System.out.println("  Because: Compiling with GCC is annoying to say the least.");
		}
	}
}