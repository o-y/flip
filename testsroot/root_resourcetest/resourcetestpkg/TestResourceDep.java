/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package resourcetestpkg;

import java.net.URL;

/**
 * 
 * 
 * @author Gima
 */
public class TestResourceDep {
	public static void test() {
		URL resource1 = TestResourceDep.class.getClassLoader().getResource("subfolder/testdata.txt");
		URL resource2 = TestResourceDep.class.getClassLoader().getResource("jarsubfolder/testjardata.txt");
		
		if (resource1 == null) {
			System.out.println("Test 5/6 FAILED: resource1==null (TestResourceDep)");
		}
		else if (resource2 == null) {
			System.out.println("Test 5/6 FAILED: resource2==null (TestResourceDep)");
		}
		else {
			System.out.println("Test 5/6 successful (TestResourceDep)");
		}
	}
}