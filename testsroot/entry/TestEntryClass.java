/* [GPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import compiletestpkg.TestCompileDep;
import classtestpkg.TestClassDep;
import jartestpkg.TestJarDep;
import resourcetestpkg.TestResourceDep;
import nativetestpkg.TestNativeDep;

/* FLIP DEPENDENCIES:
compile "testsroot/root_compiletest"
	compiletestpkg.TestCompileDep
	
classes
	testsroot/root_classtest/
	testsroot/root_jartest/pkg.jar
	
compile "testsroot/root_resourcetest/"
	resourcetestpkg.TestResourceDep
	
resources
	testsroot/resourcetest_resources/
	testsroot/resourcetest_resources/jarresource.jar
	
compile "testsroot/root_nativetest/"
	nativetestpkg.TestNativeDep
	nativetestpkg.NativeLib
	
native "nativetest_libs/"
*/

/**
 * 
 * 
 * @author Gima
 */
public class TestEntryClass {
	
	public static void main(String[] args) {
		System.out.println("Test 1/6 successful (TestEntryClass)");
		TestCompileDep.test();
		TestClassDep.test();
		TestJarDep.test();
		TestResourceDep.test();
		TestNativeDep.test();
	}
	
}
